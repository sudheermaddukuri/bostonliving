import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoadingController, ModalController } from '@ionic/angular';
import { AddComplaintModal } from '../addcomplaint/addcomplaint.page';
import { UserService } from '../user.service';

@Component({
  selector: 'app-helpdesk',
  templateUrl: 'helpdesk.page.html',
  styleUrls: ['helpdesk.page.scss']
})
export class HelpDeskPage {
  dataReturned:any;

  constructor(public userService: UserService, public http: HttpClient, public loadingController: LoadingController, public modalController: ModalController){
    this.getComplaintsList()
  }

  async presentModal() {
    const modal = await this.modalController.create({
      component: AddComplaintModal,
      componentProps: {
        "uID": this.userService.getUID(),
        "modalTitle": "Add Complaint",
        "bedNumber": ''
      }
    });
    modal.onDidDismiss().then((dataReturned) => {
      if (dataReturned !== null) {
        this.dataReturned = dataReturned.data;
      }
      this.getComplaintsList();
    });

    return await modal.present();
  }

  complaintsList: any[]=[]
  async getComplaintsList()
  {
    const loading = await this.loadingController.create({
      message: 'Loading..',
      duration: 2000
    });
    await loading.present();

    this.http.get('http://68.183.85.203:3000/api/complaints/').subscribe(data=>{
      console.log(data);
      this.complaintsList = data["data"];
      console.log('complaints == '+this.complaintsList);
      loading.dismiss();
    })
  }

}
