import { Injectable } from '@angular/core'
import { AngularFireAuth } from '@angular/fire/auth'
import { first } from 'rxjs/operators'
import { auth } from 'firebase/app'
import { AppUser } from './auth/login/AppUser.model'
import { Router } from '@angular/router'


@Injectable()
export class UserService {
	private user: AppUser

	constructor(private router: Router, private afAuth: AngularFireAuth) {

	}

	setUser(userObj: string) {
		userObj;
		//this.user = user;
	}

	getUser() {
		return this.user;
	}

	getUsername(): string {
		return this.user.displayName;
	}

	isLoggedIn(): boolean {
		this.setUser(localStorage.getItem('userref'));
		return this.user != null;
	}


	getUID(): string {
		return this.user.uid;
	}

	async logout() {
		localStorage.removeItem('user');
		localStorage.removeItem('userref');
		await this.afAuth.auth.signOut();
		this.router.navigate(['/login']);
	  }
}