import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginPageModule } from './auth/login/login.module';
import { LoginPageRoutingModule } from './auth/login/login-routing.module';

import {AngularFireModule} from '@angular/fire';
import {AngularFirestoreModule, FirestoreSettingsToken} from '@angular/fire/firestore';
import {AngularFireAuthModule, AngularFireAuth} from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { UserService } from './user.service';
import { AuthGuard } from './auth/auth.guard';

import * as firebase from 'firebase';
import { TabsPageRoutingModule } from './tabs/tabs-routing.module';
import { TabsPageModule } from './tabs/tabs.module';
import { ShareModule } from './share.module';
import { Firebase } from '@ionic-native/firebase/ngx';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { AddComplaintModal } from './addcomplaint/addcomplaint.page';
import { AddComplaintModalModule } from './addcomplaint/addcomplaint.module';
import { AngularFireFunctions } from '@angular/fire/functions';


firebase.initializeApp(environment.firebase);
@NgModule({
  declarations: [AppComponent],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, 
    TabsPageRoutingModule,
    TabsPageModule,
    LoginPageModule, 
    LoginPageRoutingModule, 
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    AngularFireAuthModule,
    ShareModule,
    HttpClientModule,
    AddComplaintModalModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: FirestoreSettingsToken, useValue: {}},
    UserService,
    AuthGuard,
    AngularFireAuth,
    Firebase,
    HttpClient,
    LoadingController,
    AddComplaintModal,
    AngularFireFunctions
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
