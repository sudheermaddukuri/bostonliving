import { Injectable } from '@angular/core'
import { Router, CanActivate, UrlTree, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router'
import { UserService } from '../user.service'
import { Observable } from 'rxjs'

@Injectable({
	providedIn: 'root'
  })
export class AuthGuard implements CanActivate {

	constructor(private router: Router, private user: UserService) {

	}

	canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
		if(this.user.isLoggedIn()) {
			return true;
		}else {
			return this.router.navigate(['/login'], { queryParams: { returnUrl: state.url }});
		}
	}
}