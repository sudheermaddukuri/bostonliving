import { Component, OnInit } from '@angular/core';

import * as firebase from 'firebase/app';
import {AlertController} from '@ionic/angular';
import {AngularFireAuth} from '@angular/fire/auth';
import {Firebase} from '@ionic-native/firebase/ngx';
import {Subscription} from 'rxjs';

import {auth} from 'firebase/app';
import { Router } from '@angular/router';
import { UserService } from '../../user.service';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireFunctions } from '@angular/fire/functions';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  username: string;
  password: string;
  phoneNumber: string;
  loading: boolean = false;
  public recaptchaVerifier;
  private authSub: Subscription;
  private verificationId: string;
  constructor(
    public afAuth: AngularFireAuth,
    public user: UserService, public router: Router,
    private alertCtrl: AlertController,
    private fireNative: Firebase,
    private afs: AngularFirestore,
    private fns: AngularFireFunctions){
      var self = this;
      this.authSub = this.afAuth.authState.subscribe(user => {
        if (user) {
          console.log('Got the user!');
        } else {
          localStorage.removeItem('user');
          localStorage.removeItem('userref');
        }
    });
  }

  ngAfterViewInit(){
    this.recaptchaVerifier = new firebase.auth.RecaptchaVerifier('recaptcha-container', {
      'size': 'invisible',
      'callback': function(response) {
        // reCAPTCHA solved, allow signInWithPhoneNumber.
        this.loginPhone();
      }
    });
  }

  ngOnInit() {
    
  }

  ngOnDestroy() {
    this.authSub.unsubscribe();
  }


  async loginPhone(): Promise<void> {
    const phone = '+91' + this.phoneNumber;
    this.loading = true;
    var result = await this.afAuth.auth.signInWithPhoneNumber(phone, this.recaptchaVerifier).then(confirmationResult => {
      this.verificationId = confirmationResult.verificationId;
    }).catch(function(error){
      console.log('error -- '+error);
    });
    await this.showPrompt(this.verificationId);
  }

  private async verifyCode(code: string, verificationId: string): Promise<void> {
    const credential = await firebase.auth.PhoneAuthProvider.credential(verificationId, code);
    this.afAuth.auth.signInAndRetrieveDataWithCredential(credential).then(res => {
      const userId = res.user.uid;
      this.afs.collection('users', ref => ref.where(`users_exists.${userId}`, "==", true).limit(1)).get().toPromise().then(clientQuerySnapshot => {
        if (!clientQuerySnapshot.empty) {
          let clientDoc = clientQuerySnapshot.docs[0];
          const clientId = clientDoc.id;
          this.afs.doc(`clients/${clientId}/users/${userId}`).valueChanges().pipe(first()).toPromise().then(user => {
            if (user) {
              localStorage.setItem('userref', JSON.stringify(user));

            }
          });
        }
      });
      localStorage.setItem('user', userId);
      this.loading = false;

    });
    

  }


  private async showPrompt(verificationId: string) {

    let promptCode = await this.alertCtrl.create({
      message: 'Type code that was received via SMS',
      inputs: [
        {
          name: 'code',
          type: 'tel',
          placeholder: 'Code'
        },
      ],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel'
        },
        {
          text: 'Verify',
          handler: data => {
            this.verifyCode(data.code, verificationId);
          }
        }
      ]
    });
    await promptCode.present();
  }

  private doLogin(): void {
    this.router.navigate(['/tabs'])
  }


  async login(){

    const {username, password, phoneNumber} = this;
    try{
        const res = await this.afAuth.auth.signInWithEmailAndPassword(username, password);
        if(res.user) {
          /*this.user.setUser({
            username,
            uid: res.user.uid,
            bedNumber: ''
          })*/
          this.router.navigate(['/tabs'])
        }
    } catch(err){
      console.dir(err)
      if(err.code === "auth/user-not-found"){
        console.log('User not found')
      }
    }
  }
}
