export interface AppUser {
    uid : string;
    ownerid?: string;
    role: string;
    email: string;
    displayName?: string;
    photoUrl?: string;
}