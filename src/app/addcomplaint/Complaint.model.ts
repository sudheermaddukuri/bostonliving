export interface Complaint{
    complaintId: number;
    uid: string;
    serviceType: string;
    deviceId: string;
    status: string;
}