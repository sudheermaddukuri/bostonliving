import { Component, Input, OnInit } from '@angular/core';
import { NavParams, ModalController, LoadingController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Complaint } from './Complaint.model';

@Component({
  selector: 'add-complaint',
  templateUrl: './addcomplaint.page.html',
  styleUrls: ['./addcomplaint.page.scss'],
})
export class AddComplaintModal {
  modelId:number;
  complaintId: number;
  complaintObj: Complaint;
  uID: string;
  constructor(private modalController: ModalController,
    private navParams: NavParams, public http: HttpClient, public loadingController: LoadingController) {

  }

  ngOnInit() {
    console.table(this.navParams);
    this.complaintObj = {} as Complaint;
  }
 
  async closeModal(onClosedData) {
    await this.modalController.dismiss(onClosedData);
  }


  async createComplaint() {
    const loading = await this.loadingController.create({
      message: 'Creating complaint..',
      duration: 2000
    });
    await loading.present();
    
    this.http.get('http://68.183.85.203:3000/api/random').subscribe(data => {
      console.log('random -- '+data);
      this.complaintId = data["randomNumber"];
      this.complaintObj.complaintId=this.complaintId;
      this.complaintObj.uid = this.uID;
      this.complaintObj.serviceType = "Bedroom";
      this.http.post('http://68.183.85.203:3000/api/complaints/addcomplaint', this.complaintObj).subscribe(data=>{
        console.log('Complaint create -- '+JSON.stringify(data));
        this.complaintId = data["data"];
        loading.dismiss();
        const onClosedData: Complaint = this.complaintObj;
        this.closeModal(onClosedData);
      })
    });
  }

}