import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
// The modal's component of the previous chapter
import {AddComplaintModal} from './addcomplaint.page';

@NgModule({
    declarations: [
      AddComplaintModal
    ],
    imports: [
      IonicModule,
      CommonModule
    ],
    entryComponents: [
        AddComplaintModal
    ]
})
export class AddComplaintModalModule {}
